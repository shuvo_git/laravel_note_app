<!-- resources/views/notes/index.blade.php -->

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">My notes</div>
                    <div class="panel-body">
                        @if($notes->isEmpty())
                            <p>
                                You have not created any notes! <a href="{{ url('create') }}">Create one</a> now.
                            </p>
                        @else
                        <!--ul class="list-group">
                            @foreach($notes as $note)
                                <li class="list-group-item">
                                    <a href="{{ url('edit', [$note->slug]) }}">
                                        {{ $note->title }}
                                    </a>
                                    <span class="pull-right">{{ $note->updated_at->diffForHumans() }}</span>
                                </li>
                            @endforeach
                        </ul-->
                        <div class="flex justify-center" id="cars" v-cloak>
                        <div class="card-columns">
                            @foreach($notes as $note)
                                <div class="card" style="width: 12rem;dislay:inline-block">
                                    <img class="card-img-top" src="{{URL('/img/note-icon.png')}}" alt="Card image cap">
                                    <div class="card-body">
                                        <!--h5 class="card-title"></h5-->
                                        <a href="{{ url('edit', [$note->slug]) }}">
                                            {{ $note->title }}
                                        </a>
                                        <span class="badge badge-secondary">{{ $note->updated_at->diffForHumans() }}</span>
                                        <!--p class="card-text"></p>
                                        <a href="#" class="btn btn-primary">Go somewhere</a-->
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

